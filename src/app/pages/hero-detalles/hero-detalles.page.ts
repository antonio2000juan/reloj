import { Component, OnInit } from '@angular/core';
import {  Animal } from '../../interfaces/hero.interface';
import { ActivatedRoute } from '@angular/router';
import { HerosServiceService } from '../../services/heros-service.service';
import { NavController, ToastController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-hero-detalles',
  templateUrl: './hero-detalles.page.html',
  styleUrls: ['./hero-detalles.page.scss'],
})
export class HeroDetallesPage implements OnInit {
  heroe: Animal = {
    nombre: '',
    nombre_c: '',
    promedio_v: 0,
    alimentacion: '',
    habitad: '',
  };

  heroeid: null;

  constructor( private route: ActivatedRoute,  private hero: HerosServiceService, private nav: NavController,
              private toastc: ToastController, private aletctrl: AlertController ) { }

  ngOnInit() {
    this.heroeid = this.route.snapshot.params['id'];
    if (this.heroeid) {
      // console.log('si llega;');
       this.loadHero();
     }

  }

  async guardarHeroe() {
    const toast = await this.toastc.create({
      message: 'Guardado',
      position: 'top',
      duration: 1500
    });

    if (this.heroeid) {
      this.hero.updateHeore(this.heroe, this.heroeid).then(() => {
        this.nav.navigateForward('/');
      } );
    }
    else {
    this.hero.addHero(this.heroe).then(() => {
    this.nav.navigateForward('/');
    toast.present();
    });
  }
  }

  loadHero() {
    this.hero.getHeroe(this.heroeid).subscribe(res => {
      console.log(res);
      this.heroe = res;
    });
   }

  async remove(idheroe: string, contenido?: string ) {
    const alert = await this.aletctrl.create({
      header: 'Seguro que deseas eliminar!',
      message: 'Animal',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Eliminar',
          cssClass: 'danger',
          handler: () => {
            this.hero.revomeHeroe(idheroe);
            // toast.present();

          }
        }
      ]
    });
    await alert.present();
  }

}
